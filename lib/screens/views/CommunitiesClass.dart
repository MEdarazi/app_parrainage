import 'package:app_parrainage/exportClass.dart';

class Communities extends StatefulWidget {
  @override
  _Communities createState() => _Communities();
}

class _Communities extends State<Communities> {
  @override
  void initState() {
    super.initState();
  }

  Widget buildCardGroups(
      BuildContext context, String namegrp, String desc, String assetImg) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Card(
            elevation: 15.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 4,
                    height: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: new AssetImage(assetImg),
                      ),
                    ),
                  ),
                ),
                ExpansionTile(
                  title: Text(
                    namegrp,
                    style: TextStyle(
                      fontSize: 25.0,
                    ),
                  ),
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          'Description du groupe',
                          style: TextStyle(fontSize: 20.0),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(desc),
                        SizedBox(
                          height: 20.0,
                        ),
                        RawMaterialButton(
                          fillColor: Colors.green,
                          splashColor: Colors.greenAccent,
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: const <Widget>[
                                Icon(
                                  Icons.group_add,
                                  color: Colors.orange,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  "Joindre le groupe",
                                  maxLines: 1,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                          onPressed: () => print("onclick join"),
                          shape: const StadiumBorder(),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // resizeToAvoidBottomPadding: false,

      appBar: new AppBar(
        centerTitle: true,
        title: Text("Communautées"),
        backgroundColor: Colors.blue[800],
      ),

      body: Container(
        // flex: 3,
        child: Column(
          children: <Widget>[
            Expanded(
              child: buildCardGroups(context, "Groupe Java",
                  "*******************", "images/java.png"),
            ),




          ],
        ),
      ),

      //  ),

      //  ),
    );
  }
}
