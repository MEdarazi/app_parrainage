import 'package:app_parrainage/exportClass.dart';

class ProfilPublic extends StatefulWidget {
  @override
  _ProfilPublic createState() => _ProfilPublic();
}

class _ProfilPublic extends State<ProfilPublic> {
  @override
  void initState() {
    super.initState();
  }

  final String _fullName = "Dupont Alain";
  final String _status = "Developpeur Dart Flutter";
  final String _bio =
      "\"Je suis passioné par le développement mobile.Je suis ouvert à tout type de proposition concernant la technologie que j'utilise\"";
  final String _viewers = "250";
  final String _numberMatch = "24";

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _buildCoverImage(screenSize),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: screenSize.height / 6.4),
                  _buildProfileImage(),
                  _buildFullName(),
                  _buildLocation(context),
                  _buildStatus(context),
                  _buildStatContainer(),
                  _buildBio(context),
                  _buildSeparator(screenSize),
                  SizedBox(height: 10.0),
                  _buildGetInTouch(context),
                  SizedBox(height: 15.0),
                  _buildSeparator(screenSize),
                  SizedBox(height: 10.0),
                  _buildIconContact(context),
                  SizedBox(height: 8.0),
                  _buildSeparator(screenSize),
                  _buildButtons(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 3.6,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/bkprofil.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _buildProfileImage() {
    return Center(
      child: Container(
        width: 140.0,
        height: 140.0,
        decoration: BoxDecoration(
          image: DecorationImage(

            image: AssetImage('images/profil.png'),
            fit: BoxFit.fill,

          ),
          borderRadius: BorderRadius.circular(80.0),
          border: Border.all(
            color: Colors.white,
            width: 10.0,
          ),
        ),
      ),
    );
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Text(
        _fullName,
        style: _nameTextStyle,
      ),
    );
  }

  Widget _buildLocation(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Text(
          "Belgique,Bruxelles",
          style: TextStyle(
            fontFamily: 'Spectral',
            color: Colors.black,
            fontSize: 17.0,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  Widget _buildStatus(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
        decoration: BoxDecoration(
          color: Theme.of(context).scaffoldBackgroundColor,
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Text(
          _status,
          style: TextStyle(
            fontFamily: 'Spectral',
            color: Colors.black,
            fontSize: 17.0,
            fontWeight: FontWeight.w300,
          ),
        ),
      ),
    );
  }

  Widget _buildStatItem(String label, String count) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      fontStyle: FontStyle.italic,
      color: Colors.grey[700],
      fontSize: 16.0,
      //fontWeight: FontWeight.w200,
    );

    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 24.0,
      fontWeight: FontWeight.bold,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          count,
          style: _statCountTextStyle,
        ),
        Text(
          label,
          style: _statLabelTextStyle,
        ),
      ],
    );
  }

  Widget _buildStatContainer() {
    return Container(
      height: 60.0,
      margin: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
          //color: Colors.white,
          ),

    );
  }

  Widget _buildBio(BuildContext context) {
    TextStyle bioTextStyle = TextStyle(
      fontFamily: 'Spectral',
      fontWeight: FontWeight.w400, //try changing weight to w500 if not thin
      fontStyle: FontStyle.italic,
      color: Color(0xFF799497),
      fontSize: 16.0,
    );

    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.all(8.0),
      child: Text(
        _bio,
        textAlign: TextAlign.center,
        style: bioTextStyle,
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Widget _buildGetInTouch(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: EdgeInsets.only(top: 8.0),
      child: Text(
        "Centres d'intérets",
        style: TextStyle(fontFamily: 'Roboto', fontSize: 16.0),
      ),
    );
  }

  Widget _buildIconContact(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            "Moyen de contact",
            style: TextStyle(fontFamily: 'Roboto', fontSize: 16.0),
          ),

          Row(
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.symmetric(vertical:5.0, horizontal:8.0),

                child: IconButton(
                  iconSize: 65.0,
                  onPressed: ()=> print("ICON MESSAGE"),
                  icon: Icon(Icons.mail,color: Colors.black,),


                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(vertical:5.0, horizontal:8.0),

                child: IconButton(
                  iconSize: 65.0,
                  onPressed: ()=> print("ICON GSM"),
                  icon: Icon(Icons.add_call,color: Colors.black,),


                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(vertical:5.0, horizontal:8.0),

                child: IconButton(
                  iconSize: 65.0,
                  onPressed: ()=> print("ICON GSM"),
                  icon: Icon(Icons.video_call,color: Colors.black,),


                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(vertical:5.0, horizontal:8.0),

                child: Icon (FontAwesomeIcons.trello,size: 55.0,color: Colors.black,),
              ),






            ],
          )
        ],
      ),
    );
  }

  Widget _buildButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: InkWell(
              onTap: () => Navigator.pushNamed(
                  context, '/home/mytechno/edit/lestechnos'),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        blurRadius: 4,
                        offset: Offset(0, 5)),
                  ],
                  border: Border.all(),
                  // color: Color(0xFF404A5C,),
                ),
                child: Center(
                  child: Text(
                    "RETOUR",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(width: 10.0),
          Expanded(
            child: InkWell(
              onTap: () => print("Message"),
              child: Container(
                height: 40.0,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.blue[700],
                        blurRadius: 4,
                        offset: Offset(0, 5)),
                  ],
                  border: Border.all(),
                  // color: Color(0xFF404A5C,),
                ),
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      "MATCHER",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class getClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();

    path.lineTo(0.0, size.height / 1.9);
    path.lineTo(size.width + 125, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
