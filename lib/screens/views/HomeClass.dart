import 'package:app_parrainage/exportClass.dart';

class RoutePages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeClass(),
    );
  }
}

class HomeClass extends StatefulWidget {
  @override
  _HomeClass createState() => _HomeClass();

  void test() {
    print("v");
  }
}

class _HomeClass extends State<HomeClass> {
  void routeBuild() {
    MaterialApp();
  }

  @override
  void initState() {
    super.initState();
  }

  Widget myItems(IconData icon, String s, int color, String route) {
    return InkWell(


      onTap: () => Navigator.pushNamed(context, route),
      splashColor: Colors.orange,
      child: Container(
        color: new Color(color),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      s,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),

                  ////////////
                  /////ICON//
                  /// /////////////
                  ///




                  /// /////////////////
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      icon,
                      color: Colors.white,
                      size: 30.0,
                    ),
                  ),




                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
  ///////////////////////////////
//////REGARDER WIDGET SPACER//////
  ////////////////////////////////
  Widget imagelogo() {
    return Expanded(
      flex: 2, // 20%
      child: Material(
        child: new Center(
          child: new Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,

            //margin: new EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/100),
            child: Image.asset(
              'images/etnic.png',
              fit: BoxFit.cover,
              height: 200.0,

              ///  width: 750.0,
            ),

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 10,
            margin: EdgeInsets.all(10),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Accueil"),
        backgroundColor: Colors.blue[800],
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            ///////////////
            //TOPDRAWER////
            ////////////////
            new UserAccountsDrawerHeader(
              accountName: Text("Edarazi Mohamed"),
              accountEmail: Text("m.edarazi@gmail.com"),
              currentAccountPicture: CircleAvatar(
                child: Image.asset('images/profil.png'),
              ),
            ),
            new ListTile(
                title: new Text("Accueil",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.notification_important),
                onTap: () {
                  Navigator.pushNamed(context, '/home');
                }),
            new ListTile(
                title: new Text("Mes matchs",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.check_circle),
                onTap: () {
                  Navigator.pushNamed(context, '/home/mymatchs');
                }),

            new ListTile(
                title: new Text("Mes Communautées",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.note),
                onTap: () {
                  Navigator.pushNamed(context, '/home/mytechno');
                }),
            new ListTile(
                title: new Text("Mon profil",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.account_circle),
                onTap: () {
                  Navigator.pushNamed(context, '/home/myprofil');
                }),
            new ListTile(
              title: new Text("Paramètres",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.extension),
             /// onTap: () {},
            ),

            new ListTile(
                title: new Text("Déconnexion",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.exit_to_app),
                onTap: () {
/////////////////////////////////////////////////////

                  Navigator.pushNamed(context, '/login');

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return RichAlertDialog(
                          //uses the custom alert dialog
                          alertTitle: richTitle("Déconnexion réussie"),
                          alertSubtitle: richSubtitle(""),

                          alertType: RichAlertType.SUCCESS,
                        );
                      });
                }

                /////////////////////////////////

                ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          imagelogo(),
          Expanded(
            flex: 2,
            child: StaggeredGridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 12.0,
              mainAxisSpacing: 12.0,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              children: <Widget>[
                myItems(Icons.add_alert, "Mes demandes", 0xFF4CAF50,'/home/mymatchs'),
                myItems(Icons.supervisor_account, "Communautées", 0xFF0277BD,'/home/communities'),
                myItems(FontAwesomeIcons.qrcode, "Mon QR-code", 0xFFF57F17,'/home/monqrcode'),
                myItems(FontAwesomeIcons.barcode, "Scanner QR-code", 0xFFB71C1C,'/home/scanqrcode'),

              ],
              staggeredTiles: [
                //vertical: MediaQuery.of(context).size.height/100

                StaggeredTile.extent(1, 120.0),
                StaggeredTile.extent(1, 120.0),
                StaggeredTile.extent(1, 120.0),
                StaggeredTile.extent(1, 120.0),
              ],
            ),
          ), // 20%
        ],
      ),
    );
  }

  Widget txtBt(String txt) {
    return Material();
  }
}
