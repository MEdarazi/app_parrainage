import 'package:app_parrainage/exportClass.dart';


class MyTechno extends StatefulWidget {
  @override
  _MyTechno createState() => _MyTechno();
}

class _MyTechno extends State<MyTechno> {
  Widget appBarTitle = new Text(
    "Mes Technologies",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();
  List<dynamic> _list;
  bool _isSearching;
  String _searchText = "";
  List searchresult = new List();

  ///////////////////////

  List<String> _tags = [];
  ///////////////////////


  _MyTechno() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    values();
   _tags.addAll(['Java', 'Android ', 'Flutter']);


  }

  void _getTags()
  {
    _tags.forEach((tag) => print(tag));
  }

  void values() {
    _list = List();
    _list.add("Amsterdam");
    _list.add("Bruxelles");
    _list.add("Maroc");
    _list.add("France");
    _list.add("Suisse");
    _list.add("Irlande");
    _list.add("Mozambique");
    _list.add("Japon");
  }

  ///////////////////////////////////////////
  //////////////////////BOX Methods////////
  /////////////////////////////////////////

  Widget myItems(IconData icon, String s, int color, String route) {
    return InkWell(


      onTap: () => Navigator.pushNamed(context, route),
      splashColor: Colors.orange,
      child: Container(
        color: new Color(color),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      s,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),

                  ////////////
                  /////ICON//
                  /// /////////////
                  ///




                  /// /////////////////
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      icon,
                      color: Colors.white,
                      size: 30.0,
                    ),
                  ),




                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  //////////////////////////
  ///////////////////////
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: globalKey,

      ///  backgroundColor: Colors.blue[800],
      appBar: buildAppBar(context),
      drawer: buildDrawer(context),

      body: _isSearching
          ? new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Flexible(
                      child: searchresult.length != 0 ||
                              _controller.text.isNotEmpty
                          ? new ListView.builder(
                              shrinkWrap: true,
                              itemCount: searchresult.length,
                              itemBuilder: (BuildContext context, int index) {
                                String listData = searchresult[index];
                                return new ListTile(
                                  title: new Text(listData.toString()),
                                );
                              },
                            )
                          : new ListView.builder(
                              shrinkWrap: true,
                              itemCount: _list.length,
                              itemBuilder: (BuildContext context, int index) {
                                String listData = _list[index];
                                return new ListTile(
                                  title: new Text(listData.toString()),
                                );
                              },
                            )),


                ],

              ),
            )
      :Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: StaggeredGridView.count(

              crossAxisCount: 2,
              crossAxisSpacing: 12.0,
              mainAxisSpacing: 12.0,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              children: <Widget>[
                myItems(Icons.add_to_photos, "Java", 0xFF4CAF50, '/home/mytechno/edit/lestechnos'),
                myItems(Icons.flag, "Dart", 0xFFF57F17, '/home/mytechno/edit/lestechnos'),
                myItems(Icons.supervisor_account, "Javascript", 0xFF0277BD, '/home/mytechno/edit/lestechnos'),
                myItems(Icons.line_style, "Python", 0xFFB71C1C, '/home/mytechno/edit/lestechnos'),
              ],
              staggeredTiles: [
                //vertical: MediaQuery.of(context).size.height/100

                StaggeredTile.extent(

                  1,
                  120.0,
                ),


                StaggeredTile.extent(1, 120.0),
                StaggeredTile.extent(1, 120.0),
                StaggeredTile.extent(1, 120.0),
              ],
            ),
          ), // 20%
        ],
      ),

      floatingActionButton: !_isSearching
          ? FloatingActionButton(
              onPressed: () => {
              Navigator.pushNamed(context, '/home/mytechno/edit'),
              },
              tooltip: 'AddTechno',
              child: Icon(
                Icons.edit,
                color: Colors.white,
              ),
            )
          : Container(),
    );
  }

  Widget buildDrawer(BuildContext c) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ///////////////
          //TOPDRAWER////
          ////////////////
          new UserAccountsDrawerHeader(
            accountName: Text("Edarazi Mohamed"),
            accountEmail: Text("m.edarazi@gmail.com"),
            currentAccountPicture: CircleAvatar(
              child: Image.asset('images/profil.png'),
            ),
          ),
          new ListTile(
              title: new Text("Accueil",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.notification_important),
              onTap: () {
                Navigator.pushNamed(context, '/home');
              }),
          new ListTile(
              title: new Text("Mes matchs",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.check_circle),
              onTap: () {
                Navigator.pushNamed(context, '/home/mymatchs');
              }),

          new ListTile(
              title: new Text("Mes Technologies",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.note),
              onTap: () {
                Navigator.pushNamed(context, '/home/mytechno');
              }),
          new ListTile(
              title: new Text("Mon profil",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.account_circle),
              onTap: () {
                Navigator.pushNamed(context, '/home/myprofil');
              }),
          new ListTile(
            title: new Text("Paramètres",
                style: new TextStyle(
                  fontSize: 16.0,
                )),
            trailing: Icon(Icons.extension),

            ///  onTap: nextToParam,
          ),

          new ListTile(
              title: new Text("Déconnexion",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.exit_to_app),
              onTap: () {
/////////////////////////////////////////////////////

                Navigator.pushNamed(context, '/login');

                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return RichAlertDialog(
                        //uses the custom alert dialog
                        alertTitle: richTitle("Déconnexion réussie"),
                        alertSubtitle: richSubtitle(""),

                        alertType: RichAlertType.SUCCESS,
                      );
                    });
              }

              /////////////////////////////////

              ),
        ],
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: appBarTitle,
        backgroundColor: Colors.blue[800],
        actions: <Widget>[
//////////////////////////////////////
          new IconButton(
            icon: icon,
            onPressed: () {
              setState(() {
                if (this.icon.icon == Icons.search) {
                  this.icon = new Icon(
                    Icons.close,
                    color: Colors.white,
                  );
                  this.appBarTitle = new TextField(
                    controller: _controller,
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Rechercher une Techno...",
                        hintStyle: new TextStyle(color: Colors.white)),
                    onChanged: searchOperation,
                  );
                  _handleSearchStart();
                } else {
                  _handleSearchEnd();
                }
              });
            },
          ),
        ]);
  }




//////////////////////////////
  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Mes Technologies",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
    });
  }

  void searchOperation(String searchText) {
    searchresult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < _list.length; i++) {
        String data = _list[i];
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchresult.add(data);
        }
      }
    }
  }
}
