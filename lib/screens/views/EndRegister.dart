import 'package:app_parrainage/exportClass.dart';

class EndRegister extends StatefulWidget {
  @override
  _EndRegister createState() => _EndRegister();
/*
  int id;
  String name;
  EndRegister(this.id, this.name);
  static List<EndRegister> getNameDepartement() {
    return <EndRegister>[
      EndRegister(1, 'AGE'),
      EndRegister(2, 'COMPTA'),

    ];
  }
}


*/
}

class _EndRegister extends State<EndRegister> {
String selected,sel;



  @override
  void initState() {
    super.initState();
  //  _dropdownItems=buildDropDownItems(_nameDepartement);
   // _selectedDepartement=_dropdownItems[0].value;
  }

 // List<EndRegister> _nameDepartement = EndRegister.getNameDepartement();
 // List<DropdownMenuItem<EndRegister>> _dropdownItems;
 // EndRegister _selectedDepartement;

  /*List<DropdownMenuItem<EndRegister>> buildDropDownItems(List depart) {
    List<DropdownMenuItem<EndRegister>> items = List();
    for (EndRegister er in depart) {
      items.add(
        DropdownMenuItem(
          value: er,
          child: Text(er.name),
        ),
      );
    }
    return items;
  }
*/
  @override
  Widget build(BuildContext context) {




    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        padding:
            EdgeInsets.only(top: 100.0, right: 20.0, left: 20.0, bottom: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Terminer l'inscription",
              style: TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
            SizedBox(
              height: 10.0,
            ),
            new Center(
              child: Image.asset(
                'images/profil.png',
                height: 152.0,
              ),
            ),
            RawMaterialButton(
              fillColor: Colors.green,
              splashColor: Colors.greenAccent,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Icon(
                      Icons.add_a_photo,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "Ajouter une Image",
                      maxLines: 1,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              onPressed: () => print("test"),
              shape: const StadiumBorder(),
            ),
            SizedBox(
              height: 20.0,
            ),
            ///
        /// /// /// /// /// /// ///////////

        DropdownButtonFormField<String>(
          decoration: InputDecoration(
            labelText: "Selectionnez le département",

            filled: true,
          ),
          value: selected,
          items: ["AGE", "IT", "COMPTA","INNOV"]
              .map((label) => DropdownMenuItem(
            child: Text(label),
            value: label,
          ))
              .toList(),
          onChanged: (v) {
            setState(() => selected = v);
          },

          // );
        ),

            SizedBox(
              height: 40.0,
            ),

             /// Select Tags///


            DropdownButtonFormField<String>(
              decoration: InputDecoration(
                labelText: "Selectionnez un ou plusieurs centres & intérêts",

                filled: true,
              ),
              value: sel,
              items: ["Java", "Javascript", "Python","C#"]
                  .map((label) => DropdownMenuItem(
                child: Text(label),
                value: label,
              ))
                  .toList(),
              onChanged: (v) {
                setState(() => sel = v);
              },

              // );
            ),





          /// //// //// //

            SizedBox(
              height: 40.0,
            ),

            TextFormField(
               keyboardType: TextInputType.number,


               decoration: InputDecoration(
                 labelText: "Numéro de téléphone",
                 filled: true,


               ),
             ),
            SizedBox(
              height: 20.0,
            ),


            SizedBox(
              height: 20.0,
            ),

            Container(

              child: RawMaterialButton(
                fillColor: Colors.blue,
                splashColor: Colors.greenAccent,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(
                        Icons.save_alt,
                        color: Colors.black,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "Sauvegarder",
                        maxLines: 1,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
                onPressed: () => print("test"),
                shape: const StadiumBorder(),
              ),
            ),

             new Container(
              margin: new EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 30),
              child: Text(
                "Version Beta",
                style:
                    TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
              ),
            ),
          ],
        ),
      ),
    );
  }




}
