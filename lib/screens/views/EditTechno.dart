import 'package:app_parrainage/exportClass.dart';

class EditTechno extends StatefulWidget {
  @override
  _EditTechno createState() => _EditTechno();
}

class _EditTechno extends State<EditTechno> {
  ///////////////////////

  ///////////////////////

  @override
  void initState() {
    super.initState();
  }

  ///Drop Down Methods///
  //////////////////////
  void dropDownMenu(BuildContext context) {
    new DropdownButton<String>(
      items: <String>['A', 'B', 'C', 'D'].map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (_) {},
    );
  }

  void onPress(Key id) {
    print('pressed $id');
  }
///////////////////////////////////////////
  //////////////////////BOX Methods////////
  /////////////////////////////////////////

  Widget myItems(IconData icon, String s, int color, String route) {
    return Stack(
      children: <Widget>[

        ///INKWELLL/////
        /////////////////
       InkWell(
         onTap: () => Navigator.pushNamed(context, route),
         splashColor: Colors.orange,
         child: Container(
           color: new Color(color),
           child: Padding(
             padding: const EdgeInsets.all(8.0),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: <Widget>[
                 Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: <Widget>[
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: Text(
                         s,
                         style: TextStyle(
                           fontSize: 15.0,
                         ),
                       ),
                     ),

                     ////////////
                     /////ICON//
                     Padding(
                       padding: const EdgeInsets.all(16.0),
                       child: Icon(
                         icon,
                         color: Colors.white,
                         size: 30.0,
                       ),
                     ),
                   ],
                 ),

               ],
             ),
           ),
         ),
        ),

        Positioned(

          child: IconButton(
            //alignment: AlignmentDirectional(0.0,-1.5),
            //alignment: Alignment.topRight,
            padding: EdgeInsets.symmetric(horizontal: 150.0, vertical: 1.0),
            icon: Icon(

              Icons.delete,
              color: Colors.black,
              size: 30.0,
            ),
           // tooltip: "DELETE",
            onPressed: () {
              print('delete succes');
            },
          ),
        ),


      ],
    );
    return InkWell(
      onTap: () => Navigator.pushNamed(context, route),
      splashColor: Colors.orange,
      child: Container(
        color: new Color(color),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      s,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),

                  ////////////
                  /////ICON//
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      icon,
                      color: Colors.white,
                      size: 30.0,
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }

  //////////////////////////
  ///////////////////////

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

        ///  backgroundColor: Colors.blue[800],
        appBar: buildAppBar(context),
        drawer: buildDrawer(context),
//////////////////////////////////
        body: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  myItems(Icons.add_to_photos, "Java", 0xFF4CAF50,
                      '/home/mytechno/edit/lestechnos'),
                  myItems(Icons.flag, "Dart", 0xFFF57F17,
                      '/home/mytechno/edit/lestechnos'),
                  myItems(Icons.supervisor_account, "Javascript", 0xFF0277BD,
                      '/home/mytechno/edit/lestechnos'),
                  myItems(Icons.line_style, "Python", 0xFFB71C1C,
                      '/home/mytechno/edit/lestechnos'),
                ],
                staggeredTiles: [
                  //vertical: MediaQuery.of(context).size.height/100

                  StaggeredTile.extent(
                    1,
                    120.0,
                  ),
                  StaggeredTile.extent(1, 120.0),
                  StaggeredTile.extent(1, 120.0),
                  StaggeredTile.extent(1, 120.0),
                ],
              ),
            ), // 20%
          ],
        ),
///////////////////////////////
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.pushNamed(context,  '/home/register/endregister/addtechno');
          }, //redirection vers la page ajout d'une techno

         // tooltip: '',
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ));
  }

  Widget buildDrawer(BuildContext c) {
    /* return Drawer(
      child: ListView(
        children: <Widget>[
          ///////////////
          //TOPDRAWER////
          ////////////////
          new UserAccountsDrawerHeader(
            accountName: Text("Edarazi Mohamed"),
            accountEmail: Text("m.edarazi@gmail.com"),
            currentAccountPicture: CircleAvatar(
              child: Image.asset('images/profil.png'),
            ),
          ),
          new ListTile(
              title: new Text("Accueil",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.notification_important),
              onTap: () {
                Navigator.pushNamed(context, '/home');
              }),
          new ListTile(
              title: new Text("Mes matchs",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.check_circle),
              onTap: () {
                Navigator.pushNamed(context, '/home/mymatchs');
              }),

          new ListTile(
              title: new Text("Mes Technologies",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.note),
              onTap: () {
                Navigator.pushNamed(context, '/home/mytechno');
              }),
          new ListTile(
              title: new Text("Mon profil",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.account_circle),
              onTap: () {
                Navigator.pushNamed(context, '/home/myprofil');
              }),
          new ListTile(
            title: new Text("Paramètres",
                style: new TextStyle(
                  fontSize: 16.0,
                )),
            trailing: Icon(Icons.extension),

            ///  onTap: nextToParam,
          ),

          new ListTile(
              title: new Text("Déconnexion",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.exit_to_app),
              onTap: () {
/////////////////////////////////////////////////////

                Navigator.pushNamed(context, '/login');

                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return RichAlertDialog(
                        //uses the custom alert dialog
                        alertTitle: richTitle("Déconnexion réussie"),
                        alertSubtitle: richSubtitle(""),

                        alertType: RichAlertType.SUCCESS,
                      );
                    });
              }

              /////////////////////////////////

              ),
        ],
      ),
   */ // );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: new Text("Editer Ses Technologies"),
        backgroundColor: Colors.blue[800],
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.orange[700],
              size: 35.0,
            ),
            onPressed: _showDialog,
          )
//////////////////////////////////////
        ]);
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Sauvegarder vos modification"),
          content: new Text(
              "Etes-vous sur de vouloir enrengistrer les paramètres actuels ?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              splashColor: Colors.green,
              child: new Text(
                "Oui",
                style: new TextStyle(fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              splashColor: Colors.red,
              child: new Text(
                "Non",
                style: new TextStyle(fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
