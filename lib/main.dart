

import 'exportClass.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      routes: {
        '/login': (context) => Login(),
        '/resetpassword': (context) => ResetPasswod(),
        '/register': (context) => Register(),
        '/home': (context) => HomeClass(),
        '/home/scanqrcode': (context) => ScanQrcode(),
        '/home/monqrcode': (context) => MyQrcode(),
        '/home/mymatchs': (context) => MyMatchs(),
        '/home/myprofil': (context) => MyProfil(),
        '/home/profilpublic': (context) => ProfilPublic(),
        '/home/mytechno': (context) => MyTechno(),
        '/home/mytechno/edit': (context) => EditTechno(),
        '/home/mytechno/edit/lestechnos': (context) => Technologies(),
        '/home/register/endregister/addtechno': (context) => AddTechno(),
        '/home/register/endregister': (context) => EndRegister(),
        '/home/communities': (context) => Communities(),
      },

      title: 'App-Parrainage',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Login(),
      //home: HomeClass(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[],
        ),
      ),
    );
  }
}
