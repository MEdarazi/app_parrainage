import 'package:app_parrainage/exportClass.dart';

class ResetPasswod extends StatefulWidget {
  @override
  _ResetPasswod createState() => _ResetPasswod();
}

class _ResetPasswod extends State<ResetPasswod> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Réinitialiser le mot de passe"),
        backgroundColor: Colors.blue[800],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Center(
              child: CircleAvatar(
                maxRadius: 75,
                backgroundColor: Colors.grey,
                child: Image.asset(
                  'images/reset.png',

                  //fit: BoxFit.fill,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Text(
                  "Entrez le même e-mail avec lequelle vous vous êtes inscrit",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    fontFamily: 'Roboto',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new TextFormField(


                decoration: new InputDecoration(
                  prefixIcon: Icon(Icons.mail),
                  labelText: "Entrez votre E-mail",
                  fillColor: Colors.black,
                  border: new OutlineInputBorder(

                    borderRadius: new BorderRadius.circular(25.0),
                    borderSide: new BorderSide(),
                  ),
                  //fillColor: Colors.green
                ),
              ),
            ),



            buildButtonSend(),
              //  SizedBox(height:  MediaQuery.of(context).size.height/15)





          ],
        ),
      ),
    );
  }

  Widget buildButtonSend() {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: MaterialButton(
        //height: 56.0,
        height: MediaQuery.of(context).size.height / 15,
        splashColor: Colors.blue[800],
        color: Colors.green,
        onPressed: () {
          Navigator.pushNamed(context, '');
        },
        child: Text(
          "Envoyer",
          style: new TextStyle(
            fontSize: 19.9,
            color: Colors.white,
          ),
        ),
        minWidth: 200.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(23.0),
        ),
      ),
    );
  }
}
