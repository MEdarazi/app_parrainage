
import  'package:app_parrainage/exportClass.dart';
class MyMatchs extends StatefulWidget {



  @override
  _MyMatchs createState() => _MyMatchs();



}


class _MyMatchs extends State<MyMatchs> {

  //HomeClass h = new HomeClass();

List<Widget>containers = [


  Container(
    child: Text(
      "Mes demandes",
    ),
  ),
  Container(
    child: Text(
      "En attente",
    ),
  ),
  Container(
    child: Text(
      "Confirmées",
    ),

  )

  /////////////
];




  void nextToMatchs(){
     Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => MyMatchs(),
      ),
    );
  }
  @override
  void initState() {

    super.initState();
  }



  @override
  Widget build(BuildContext context){

    return DefaultTabController(
      length: 3,
      child: Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Mes Matchs"),
        bottom: TabBar(
          indicatorColor: Colors.green,
          labelColor: Colors.white,
            unselectedLabelColor:  Colors.blueGrey[400],
          ///
          unselectedLabelStyle: TextStyle(

            fontSize: 13.0,
          ),
          tabs:<Widget>[
            Tab(

              text: "Mes demandes",




              icon: Icon(Icons.library_books),
            ),
            Tab(
              text: "En attente",
              icon: Icon(Icons.pause_circle_filled),
            ),
            Tab(
              text: "Confirmées",
              icon: Icon(Icons.check_circle),

            ),
          ]
        ),
        backgroundColor: Colors.blue[800],
      ),
        drawer: new Drawer(

          child: ListView(
            children: <Widget>[


              ///////////////
              //TOPDRAWER////
              ////////////////
              new UserAccountsDrawerHeader(
                accountName: Text("Edarazi Mohamed"),
                accountEmail: Text("m.edarazi@gmail.com"),
                currentAccountPicture: CircleAvatar(
                  child: Image.asset('images/profil.png'),
                ),

              ),
              new ListTile(

                  title: new Text(
                      "Accueil",
                      style: new TextStyle(
                        fontSize: 16.0,
                      )

                  ),
                  trailing: Icon(Icons.notification_important),
                  onTap: (){
                    Navigator.pushNamed(context, '/home');
                  }

              ),
              new ListTile(

                  title: new Text(
                      "Mes matchs",
                      style: new TextStyle(
                        fontSize: 16.0,
                      )

                  ),
                  trailing: Icon(Icons.check_circle),
                  onTap: (){
                    Navigator.pushNamed(context, '/home/mymatchs');
                  }
              ),

              new ListTile(

                  title: new Text(
                      "Mes Technologies",
                      style: new TextStyle(
                        fontSize: 16.0,
                      )

                  ),
                  trailing: Icon(Icons.note),
                  onTap: (){
                    Navigator.pushNamed(context, '/home/mytechno');
                  }
              ),
              new ListTile(

                  title: new Text(
                      "Mon profil",
                      style: new TextStyle(
                        fontSize: 16.0,
                      )

                  ),
                  trailing: Icon(Icons.account_circle),
                  onTap: (){
                    Navigator.pushNamed(context, '/home/myprofil');
                  }
              ),
              new ListTile(

                title: new Text(
                    "Paramètres",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )

                ),
                trailing: Icon(Icons.extension),
              ///  onTap: nextToParam,
              ),


              new ListTile(

                  title: new Text(
                      "Déconnexion",
                      style: new TextStyle(
                        fontSize: 16.0,
                      )

                  ),
                  trailing: Icon(Icons.exit_to_app),
                  onTap:  (){





/////////////////////////////////////////////////////

                    Navigator.pushNamed(context, '/login');

                    showDialog(


                        context: context,
                        builder: (BuildContext context) {
                          return RichAlertDialog( //uses the custom alert dialog
                            alertTitle: richTitle("Déconnexion réussie"),
                            alertSubtitle: richSubtitle(""),

                            alertType: RichAlertType.SUCCESS,
                          );
                        }
                    );

                  }

                /////////////////////////////////


              ),
            ],
          ),

        ),
      body: TabBarView(
        children: containers
      ),

      ),




    );
  }



}




