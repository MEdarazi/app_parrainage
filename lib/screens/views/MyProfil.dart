import 'package:app_parrainage/exportClass.dart';

class MyProfil extends StatefulWidget {
  @override
  _MyProfil createState() => _MyProfil();
}

class _MyProfil extends State<MyProfil> {
  bool _isHidden = true;

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  void addImage() {}

  @override
  Widget build(BuildContext context) {
    double _size = MediaQuery.of(context).size.width / 0.5;
    double _size1 = MediaQuery.of(context).size.height / 350;
    double _size2 = MediaQuery.of(context).size.height / 350;
    double _size3 = MediaQuery.of(context).size.height / 350;

    return Scaffold(
      ///resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: new AppBar(
        centerTitle: true,
        title: Text("Mon Profil"),
        backgroundColor: Colors.blue[800],
      ),

      ///  resizeToAvoidBottomInset: true,
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            ///////////////
            //TOPDRAWER////
            ////////////////
            new UserAccountsDrawerHeader(
              accountName: Text("Edarazi Mohamed"),
              accountEmail: Text("m.edarazi@gmail.com"),
              currentAccountPicture: CircleAvatar(
                child: Image.asset('images/profil.png'),
              ),
            ),
            new ListTile(
                title: new Text("Accueil",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.notification_important),
                onTap: () {
                  Navigator.pushNamed(context, '/home');
                }),
            new ListTile(
                title: new Text("Mes matchs",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.check_circle),
                onTap: () {
                  Navigator.pushNamed(context, '/home/mymatchs');
                }),

            new ListTile(
                title: new Text("Mes Technologies",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.note),
                onTap: () {
                  Navigator.pushNamed(context, '/home/mytechno');
                }),
            new ListTile(
                title: new Text("Mon profil",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.account_circle),
                onTap: () {
                  Navigator.pushNamed(context, '/home/myprofil');
                }),
            new ListTile(
              title: new Text("Paramètres",
                  style: new TextStyle(
                    fontSize: 16.0,
                  )),
              trailing: Icon(Icons.extension),
              //   onTap: ,
            ),

            new ListTile(
                title: new Text("Déconnexion",
                    style: new TextStyle(
                      fontSize: 16.0,
                    )),
                trailing: Icon(Icons.exit_to_app),
                onTap: () {
/////////////////////////////////////////////////////

                  Navigator.pushNamed(context, '/login');

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return RichAlertDialog(
                          //uses the custom alert dialog
                          alertTitle: richTitle("Déconnexion réussie"),
                          alertSubtitle: richSubtitle(""),

                          alertType: RichAlertType.SUCCESS,
                        );
                      });
                }

                /////////////////////////////////

                ),
          ],
        ),
      ),

      body: SingleChildScrollView(

          ///alignment: Alignment.center,

          child: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,

            //margin: new EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/100),
            child: Image.asset(
              'images/profil.png',
              fit: BoxFit.cover,
              height: 200.0,

              ///  width: 750.0,
            ),

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 10,
            margin: EdgeInsets.all(30),
          ),

          ///   Expanded(
          ///   flex: 1,

          RawMaterialButton(
            fillColor: Colors.green,
            splashColor: Colors.greenAccent,
            child: Padding(
              ///padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              padding: EdgeInsets.all(10.0),

              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: const <Widget>[
                  Icon(
                    Icons.add_a_photo,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    "Ajouter une Image",
                    maxLines: 1,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            onPressed: addImage,
            shape: const StadiumBorder(),
          ),

          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                /// /////////////////////
                SizedBox(
                  height: 40.0,
                ),
                buildTextField("Email"),
                SizedBox(
                  height: 20.0,
                ),
                buildTextField("Nom"),
                SizedBox(
                  height: 20.0,
                ),
                buildTextField("Prénom"),
                SizedBox(
                  height: 20.0,
                ),
                buildTextField("Modifier Son Mot de Passe"),
                SizedBox(
                  height: 20.0,
                ),

                /// /////////////////////////////////
                ///
                ///

                Column(
                  children: <Widget>[
                    buildIconButton(context),
                    SizedBox(
                      height: 20.0,
                    ),
                    buildButtonSave(),
                    SizedBox(
                      height: 20.0,
                    ),
                    buildButtonCancel(),
                  ],
                ),
              ],
            ),
          ),

          ///CALL FIELD METHODS///

          ////////////////////////
        ],
      )),
    );
  }

  ///Methods Build FIELD/////
  /////////////////////////
  /// ////////////////////////
  Widget buildTextField(String hintText) => TextField(
        style: new TextStyle(
          color: Colors.black,
        ),
        decoration: InputDecoration(
          fillColor: Colors.grey[400],
          filled: true,
          hintText: hintText,
          hintStyle: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          prefixIcon: hintText == "Email"
              ? Icon(Icons.mail)
              : hintText == "Nom" ||
                      hintText == "Prénom" ||
                      hintText == "Nom d'utilisateur"
                  ? Icon(Icons.account_circle)
                  : Icon(Icons.https),
          suffixIcon: hintText == "Modifier Son Mot de Passe"
              ? IconButton(
                  onPressed: _toggleVisibility,
                  icon: _isHidden
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                )
              : null,
        ),
        obscureText: hintText == "Modifier Son Mot de Passe" ? _isHidden : false,

        //////////////////////
      );

  /// METHODS BUTTON SAVE///
  ///
  Widget buildButtonSave() {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: MaterialButton(
        //height: 56.0,
        height: MediaQuery.of(context).size.height / 15,
        splashColor: Colors.blue[800],
        color: Colors.green,
        onPressed: () {
          Navigator.pushNamed(context, '/home');
        },
        child: Text(
          "Sauvegarder",
          style: new TextStyle(
            fontSize: 20.0,
            color: Colors.white,
          ),
        ),
        minWidth: 200.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(23.0),
        ),
      ),
    );
  }

  Widget buildButtonCancel() {
    return MaterialButton(
      //height: 56.0,
      height: MediaQuery.of(context).size.height / 15,
      splashColor: Colors.blue[800],
      color: Colors.blueGrey,
      onPressed: () {
        Navigator.pushNamed(context, '/home');
      },
      child: Text(
        "Annuler",
        style: new TextStyle(
          fontSize: 20.0,
          color: Colors.white,
        ),
      ),
      minWidth: 200.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(23.0),
      ),
    );
  }

  Widget buildIconButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Text(
              "Moyen de contact",
              style: TextStyle(fontFamily: 'Roboto', fontSize: 20.0,fontWeight: FontWeight.bold),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5.0, horizontal: 8.0),
                  child: IconButton(
                    iconSize: 65.0,
                    onPressed: () => print("ICON MESSAGE"),
                    icon: Icon(
                      Icons.mail,
                      color: Colors.black,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5.0, horizontal: 8.0),
                  child: IconButton(
                    iconSize: 65.0,
                    onPressed: () => print("ICON GSM"),
                    icon: Icon(
                      Icons.add_call,
                      color: Colors.black,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5.0, horizontal: 8.0),
                  child: IconButton(
                    iconSize: 65.0,
                    onPressed: () => print("ICON GSM"),
                    icon: Icon(
                      Icons.video_call,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                // margin: ma,
                  child: Padding(

                    padding: const EdgeInsets.symmetric(
                        vertical: 5.0, horizontal: 8.0),
                    child: IconButton(
                      onPressed: ()=> print("Ca mlarche poto"),
                      icon: Icon(FontAwesomeIcons.trello,
                        size: 55.0,
                         color: Colors.black,
                      ),

                    //  FontAwesomeIcons.trello,
                   //   size: 55.0,
                     // color: Colors.black,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
