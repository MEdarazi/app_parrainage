import 'package:app_parrainage/exportClass.dart';


class AddTechno extends StatefulWidget {
  @override
  _AddTechno createState() => _AddTechno();
}

class _AddTechno extends State<AddTechno> {
  Widget appBarTitle = new Text(
    "Ajout d'une technologie",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();
  List<dynamic> _list;
  bool _isSearching;
  String _searchText = "";
  List searchresult = new List();

  ///////////////////////

  List<String> _tags = [];
  ///////////////////////


  _AddTechno() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    values();
    _tags.addAll(['Java', 'Android ', 'Flutter']);


  }

  void _getTags()
  {
    _tags.forEach((tag) => print(tag));
  }

  void values() {
    _list = List();
    _list.add("Java");
    _list.add("Python");
    _list.add("Mathématique");
    _list.add("Javascript");

  }

  ///////////////////////////////////////////
  //////////////////////BOX Methods////////
  /////////////////////////////////////////

  Widget myItems(IconData icon, String s, int color, String route) {
    return InkWell(

      onTap: () => Navigator.pushNamed(context, route),
      splashColor: Colors.orange,
      child: Container(
        color: new Color(color),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      s,
                      style: TextStyle(
                        fontSize: 15.0,
                      ),
                    ),
                  ),

                  ////////////
                  /////ICON//

                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Icon(
                      icon,
                      color: Colors.white,
                      size: 30.0,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  //////////////////////////
  ///////////////////////
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: globalKey,

      ///  backgroundColor: Colors.blue[800],
      appBar: buildAppBar(context),

      body: buildStackAdd(context),
      /* body: _isSearching
          ? new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Flexible(
                child: searchresult.length != 0 ||
                    _controller.text.isNotEmpty
                    ? new ListView.builder(

                  shrinkWrap: true,
                  itemCount: searchresult.length,
                  itemBuilder: (BuildContext context, int index) {
                    String listData = searchresult[index];
                    return new ListTile(
                      title: new Text(listData.toString()),
                    );
                  },
                )
                    : new ListView.builder(

                  shrinkWrap: true,
                  itemCount: _list.length,
                  itemBuilder: (BuildContext context, int index) {
                    String listData = _list[index];
                    return new ListTile(
                      title: new Text(listData.toString()),
                    );
                  },
                )),




          ],

        ),
      )
          :Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[

        ],
      ),
*/


    );
  }


  Widget buildAppBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: appBarTitle,
        backgroundColor: Colors.blue[800],
        actions: <Widget>[
//////////////////////////////////////
          new IconButton(
            icon: icon,
            onPressed: () {
              setState(() {
                if (this.icon.icon == Icons.search) {
                  this.icon = new Icon(
                    Icons.close,
                    color: Colors.white,
                  );
                  this.appBarTitle = new TextField(
                    controller: _controller,
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    decoration: new InputDecoration(
                        prefixIcon: new Icon(Icons.search, color: Colors.white),
                        hintText: "Ajouter une Techno...",
                        hintStyle: new TextStyle(color: Colors.white)),
                    onChanged: searchOperation,
                  );
                  _handleSearchStart();
                } else {
                  _handleSearchEnd();
                }
              });
            },
          ),
        ]);
  }


//////////////////////////////
  /// //////////////////
  ///Logo add techno///

  Widget buildStackAdd(BuildContext context){

    return _isSearching ? Stack(

      //key: globalKey,




      children: <Widget>[

        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Flexible(
                child: searchresult.length != 0 ||
                    _controller.text.isNotEmpty
                    ? new ListView.builder(

                  shrinkWrap: true,
                  itemCount: searchresult.length,
                  itemBuilder: (BuildContext context, int index) {
                    String listData = searchresult[index];
                    return new ListTile(
                      title: new Text(listData.toString()),
                    );
                  },
                )
                    : new ListView.builder(

                  shrinkWrap: true,
                  itemCount: _list.length,
                  itemBuilder: (BuildContext context, int index) {
                    String listData = _list[index];
                    return new ListTile(
                      title: new Text(listData.toString()),
                    );
                  },
                )),




          ],

        ),
        Row(
          //mainAxisAlignment: MainAxisAlignment.end,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // Positioned(

            Padding(
              padding: const EdgeInsets.all(0.0),
              child: IconButton(
                //alignment: AlignmentDirectional(0.0,-1.5),
                //alignment: Alignment.center,
                //padding: new EdgeInsets.all(0.0),
                padding: EdgeInsets.only(left: 350),

                // padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
                icon: Icon(

                  Icons.add_circle,
                  color: Colors.green,
                  size: 30.0,
                ),
                // tooltip: "DELETE",
                onPressed: () {
                  print('Add techno');
                },
              ),
            ),


            ///  ),

          ],
        )


      ],

    )


        : Center(
//padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 300.0),

      child: Column(

        mainAxisSize: MainAxisSize.min,

        children: <Widget>[


          Container(

            //margin: const EdgeInsets.only(left: 225.0, bottom: 0.0),
            //margin: new EdgeInsets.all(-150.0),
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 25.0),

            child: RawMaterialButton(

              fillColor: Colors.green,
              splashColor: Colors.greenAccent,
              child: Padding(
                //padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                padding: EdgeInsets.all(10.0),

                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Icon(
                      Icons.add_circle,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "Ajouter la techno",
                      maxLines: 1,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              onPressed:()=> print(''),
              shape: const StadiumBorder(),


            ),
          ),

          ///btCANCEL/////
          ///
          ///
          ///


          Container(
            // margin: const EdgeInsets.only(left: 225.0, bottom: 0.0),
            //margin: new EdgeInsets.all(-150.0),

            child: RawMaterialButton(

              fillColor: Colors.grey,
              splashColor: Colors.blue,
              child: Padding(
                //padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                padding: EdgeInsets.all(10.0),

                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      "Annuler",
                      maxLines: 1,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              onPressed:()=> Navigator.pushNamed(context, '/home/mytechno/edit'),
              shape: const StadiumBorder(),


            ),
          ),
        ],
      ),
    );
  }



  /////////////////////////////
  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Ajout d'une technologie",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
    });
  }

  void searchOperation(String searchText) {
    searchresult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < _list.length; i++) {
        String data = _list[i];
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchresult.add(data);
        }
      }
    }
  }
}
