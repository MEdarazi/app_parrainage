import 'package:app_parrainage/exportClass.dart';

class Technologies extends StatefulWidget {
  @override
  _Technologies createState() => _Technologies();
}

class _Technologies extends State<Technologies> {
  List names = [
    "Edarazi Mohamed",
    "Jack Dupont",
    "Bernard Lecompte",
    "Fred Ghislain",
    "Abdel",
    "Zak"
  ];
  List yearexperience = [
    "2 ans",
    "5 ans",
    "15 ans",
    "8 ans",
    "2 ans",
    "12 ans"
  ];

  Widget appBarTitle = new Text(
    "Technologies",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: Colors.white,
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      ///  backgroundColor: Colors.blue[800],
      appBar: buildAppBar(context),
      // drawer: buildDrawer(context),

      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Nom de la Techno",
            style: TextStyle(
              fontSize: 28.0,
              color: Colors.black,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          CircleAvatar(
            radius: 60.0,
            backgroundColor: Colors.white,
            child: Image.asset(
              'images/java.png',
              height: 250,
            ),

            /* child: Image.asset('images/java.png',
                    height: 200,
                    ),*/
          ),

          SizedBox(
            height: 20.0,
          ),

          Text(
            "Lien WikiTechno",
            style: TextStyle(
              fontSize: 20.0,
              fontStyle: FontStyle.italic,
            ),
          ),



          SizedBox(height: 8.0),

          //  new SingleChildScrollView(
          Expanded(
         child: buildListView(context),
          ),

          // ),

          /*   new Container(
               decoration: ShapeDecoration(
                 color: Colors.black,
                 shape: Border.all(
                   color: Colors.teal,
                   width: 150.0,
                 ),
               ),
              ),*/
          // buildTags(context),
        ],
      ),
    );
  }

  Widget buildListView(BuildContext c) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount: names.length,
          shrinkWrap: true,

          itemBuilder: (BuildContext context, int index) => Container(
            width: MediaQuery.of(context).size.width,
           padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            child: Card(
              elevation: 8.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0),
              ),
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 55.0,
                          height: 55.0,
                          child: CircleAvatar(
                            child: Image.asset(
                              'images/profil.png',
                              // height: 250,
                            ),
                          ),
                        ),
                        SizedBox(width: 6.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              names[index],
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              "Année d'expérience: " + yearexperience[index],
                              style: TextStyle(
                                color: Colors.grey[500],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding:
                          EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/home/profilpublic');
                        },
                        color: Colors.blue[500],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Text(
                          "Voir Profil",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),

    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
        centerTitle: true,
        title: appBarTitle,
        backgroundColor: Colors.blue[800],
        actions: <Widget>[
//////////////////////////////////////
        ]);
  }

//////////////////////////////

}
