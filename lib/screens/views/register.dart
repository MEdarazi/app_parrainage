
import  'package:app_parrainage/exportClass.dart';
class Register extends StatefulWidget {



  @override
  _Register createState() => _Register();
}


class _Register extends State<Register> {



  bool _isHidden = true;

  void _toggleVisibility(){
    setState(() {
      _isHidden = !_isHidden;
    });
  }




  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 100.0, right: 20.0, left: 20.0, bottom: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,

          children: <Widget>[

            new Container(

              child: Image.asset('images/profil.png',
                height: 152.0,

              ),
            ),
            SizedBox(height: 40.0,),
            Text(
              "Inscription",
              style: TextStyle(
                  fontSize: 31.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black
              ),
            ),


            /////////////////////////////////////
            /////////////////////////////////
            ///ADDSCROLLVIEW///

            SizedBox(height: 40.0,),
            buildTextField("Email"),
            SizedBox(height: 20.0,),
            buildTextField("Nom"),
            SizedBox(height: 20.0,),
            buildTextField("Prénom"),
            SizedBox(height: 20.0,),
            buildTextField("Mot de Passe"),
            SizedBox(height: 20.0,),



            ////////////////////////////////
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[

                ],
              ),
            ),
            SizedBox(height: 50.0),
            buildButtonContainer(),
            SizedBox(height: 10.0,),
            Container(
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    SizedBox(width: 10.0,),



                  ],
                ),

              ),

            ),
            new Container(
              margin: new EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/30),
              child: Text("Version Beta", style: TextStyle(
                  color:  Colors.grey,
                  fontStyle: FontStyle.italic
              ),

              ),
            ),

          ],

        ),
      ),
    );
  }

  Widget buildTextField(String hintText){


    return TextField(
      style: new TextStyle(
        color: Colors.black,
      ),
      decoration: InputDecoration(
        fillColor: Colors.orange[200],filled: true,
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.black,
          fontSize: 18.0,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),


        prefixIcon: hintText == "Email"  ? Icon(Icons.mail) : hintText =="Nom"  || hintText =="Prénom" ||  hintText =="Nom d'utilisateur" ? Icon(Icons.account_circle) :Icon(Icons.https) ,

        suffixIcon: hintText == "Mot de Passe" ? IconButton(
          onPressed: _toggleVisibility,
          icon: _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
        ) : null,
      ),
      obscureText: hintText == "Mot de Passe" ? _isHidden : false,



      //////////////////////
    );
  }



  Widget buildButtonContainer(){

    return MaterialButton(
      //height: 56.0,
      height: MediaQuery.of(context).size.height/15,
      splashColor: Colors.blue[800],
      color: Colors.green,
      onPressed: (){
        Navigator.pushNamed(context, '/home/register/endregister');
      },
      child: Text(
        "Suivant",
        style: new TextStyle(
          fontSize: 20.0,
          color: Colors.white,

        ),
      ),
      minWidth: 400.0,
      shape: RoundedRectangleBorder(

        borderRadius: BorderRadius.circular(23.0),

      ),
    );

  }

}




