
import  'package:app_parrainage/exportClass.dart';
class Login extends StatefulWidget {



  @override
  _Login createState() => _Login();
}


class _Login extends State<Login> {




  bool _isHidden = true;

  void _toggleVisibility(){
    setState(() {
      _isHidden = !_isHidden;
    });

  }


  void nextToRegister(){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => Register(),
      ),
    );
  }


  void nextToPasswordForgot(){
   /* Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => ForgotPassword(),
      ),
    );*/
  }
  @override
  void initState() {

    super.initState();

  }



  @override
  Widget build(BuildContext context){



    return Scaffold(


      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 100.0, right: 20.0, left: 20.0, bottom: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              child: Image.asset('images/etnic.png',
                height: 152.0,

              ),
            ),
            SizedBox(height: 40.0,),
            Text(
              "Connexion",
              style: TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black
              ),
            ),
            SizedBox(height: 40.0,),
            buildTextField("Email"),
            SizedBox(height: 20.0,),
            buildTextField("Mot de Passe"),
            SizedBox(height: 20.0,),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new OutlineButton(
                    shape: StadiumBorder(),
                    textColor: Colors.blue[900],
                    child: Text("Mot de Passe oublié ?"),
                    borderSide: BorderSide(
                        color: Colors.white, style: BorderStyle.solid,
                        width: 1),
                    onPressed: ()=>Navigator.pushNamed(context, '/resetpassword'),
                  ),
                ],
              ),
            ),
            SizedBox(height: 50.0),
            buildButtonContainer(),
            SizedBox(height: 10.0,),
            Container(
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Pas de Compte ?"),
                   // SizedBox(width: 8.0,),
                    SizedBox(width: MediaQuery.of(context).size.width/30,),
                   // MediaQuery.of(context).size.height/30
                    new OutlineButton(
                      shape: StadiumBorder(),
                      textColor: Colors.blue[900],
                      child: Text("S'inscrire"),
                      borderSide: BorderSide(
                          color: Colors.white, style: BorderStyle.solid,
                          width: 1),
                      onPressed: nextToRegister,
                    )
                    /*  Text("S'inscrire",
          style: TextStyle(  color: Colors.yellow[700],
          ),
         // recorgnizer: new TapGestureRecognizer()..onTap = nextToRegister,

        ),*/



                  ],
                ),

              ),

            ),
            new Container(

              //margin: new EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/50),

              child: Text("Version Beta", style: TextStyle(
                  color:  Colors.grey,
                  fontStyle: FontStyle.italic
              ),

              ),
            ),

          ],

        ),
      ),
    );
  }

  Widget buildTextField(String hintText){
    return TextField(
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          color: Colors.black,
          fontSize: 16.0,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        prefixIcon: hintText == "Email" ? Icon(Icons.email) : Icon(Icons.lock),
        suffixIcon: hintText == "Mot de Passe" ? IconButton(
          onPressed: _toggleVisibility,
          icon: _isHidden ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
        ) : null,
      ),
      obscureText: hintText == "Mot de Passe" ? _isHidden : false,
    );
  }

  Widget buildButtonContainer(){
    return MaterialButton(
      //height: 56.0,
      height: MediaQuery.of(context).size.height/15,
      splashColor: Colors.green[800],
      color: Colors.blue[800],
      onPressed: nextToHome,
      child: Text(
        'Connexion',
        style: new TextStyle(
          fontSize: 20.0,
          color: Colors.white,

        ),
      ),
      minWidth: 400.0,
      shape: RoundedRectangleBorder(

        borderRadius: BorderRadius.circular(23.0),

      ),
    );

  }


  void nextToHome(){
     Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => HomeClass(),
      ),
    );
  }

}




